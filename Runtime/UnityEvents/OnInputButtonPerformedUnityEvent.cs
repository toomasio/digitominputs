﻿using System.Collections;
using System.Collections.Generic;
using DigitomEvents;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DigitomInputs
{
    public class OnInputButtonPerformedUnityEvent : UserInputAction<bool>
    {
        [SerializeField] protected UnityEventContainer onPerformedEvents;

        protected override void Awake()
        {
            base.Awake();
        }

        protected override void OnInputPerformed(InputAction.CallbackContext ctx)
        {
            onPerformedEvents.unityEvents.Invoke();
        }
    }
}


