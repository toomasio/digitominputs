﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DigitomInputs.Runtime
{
    public class InputSystemMapHandler : MonoBehaviour
    {
        [SerializeField] private PlayerInput input;

        public void EnableMap(string mapName)
        {
            EnableMap(mapName, true);
        }

        public void DisableMap(string mapName)
        {
            EnableMap(mapName, false);
        }

        public void EnableMap(string mapName, bool enable)
        {
            var map = input.actions.FindActionMap(mapName);
            if (map != null)
            {
                if (enable)
                    map.Enable();
                else
                    map.Disable();

                Debug.Log(gameObject.name + "=>" + map.name + " : " + map.enabled);
            }
             
            
        }
    }
}


