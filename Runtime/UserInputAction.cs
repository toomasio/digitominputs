﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DigitomInputs
{
    public abstract class UserInputAction<T> : UserInputBase
        where T : struct
    {
        [SerializeField] protected InputActionReference inputReference;
        protected InputAction action;

        public T InputValue { get; protected set; }

        protected virtual void Awake()
        {
            action = GetInputAction();
        }

        protected virtual void OnEnable()
        {
            SubscribeInput();
        }

        protected virtual void OnDisable()
        {
            UnSubscribeInput();
        }

        protected virtual InputAction GetInputAction()
        {
            if (input == null)
                return inputReference.action;
            return action = input.actions.FindAction(inputReference.action.id);
        }

        protected virtual void SubscribeInput()
        {
            action.Enable();
            action.performed += OnInputPerformed;
            action.canceled += OnInputCanceled;
        }

        protected virtual void UnSubscribeInput()
        {
            action.performed -= OnInputPerformed;
            action.canceled -= OnInputCanceled;
        }

        protected virtual void OnInputPerformed(InputAction.CallbackContext ctx)
        {
            InputValue = ctx.ReadValue<T>();
        }

        protected virtual void OnInputCanceled(InputAction.CallbackContext ctx)
        {
        }

    }
}