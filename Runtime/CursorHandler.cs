﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DigitomInputs.Runtime
{
    public class CursorHandler : MonoBehaviour
    {
        public void CursorLockModeNone()
        {
            Cursor.lockState = CursorLockMode.None;
        }

        public void CursorLockModeLocked()
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        public void CursorLockModeConfined()
        {
            Cursor.lockState = CursorLockMode.Confined;
        }

        public void LockCursor(CursorLockMode lockMode)
        {
            Cursor.lockState = lockMode;
        }

        public void CursorVisible(bool visible)
        {
            Cursor.visible = visible;
        }
    }
}


