﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DigitomInputs
{
    public abstract class UserInputBase : MonoBehaviour
    {
        [SerializeField] protected PlayerInput input;
    }
}